# dinoLife

## getting started
2d platformer made with godot

play in your browser:
https://fiehra.itch.io/dinolife

![dinolife](assets/screenshot1.png?raw=true "dinoLife")
![dinolife](/assets/screenshot2.png?raw=true "dinoLife")
![dinolife](/assets/screenshot3.png?raw=true "dinoLife")

## license
MIT

## poject status
published

*might be getting some improvements
