class_name FrictionComponent
extends Node

@export var actor: Node2D
@export var FRICTION: int = 1000
@export var AIRFRICTION: int = 400

func apply_friction(delta: float) -> void:
	if actor.is_on_floor():
		actor.velocity.x = move_toward(actor.velocity.x, 0, FRICTION * delta)
	else:
		actor.velocity.x = move_toward(actor.velocity.x, 0, AIRFRICTION * delta)

