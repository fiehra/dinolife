class_name GravityComponent
extends Node

@export var actor: Node2D
@export var jump_height : float = 75.0
@export var jump_time_to_peak : float = 0.5
@export var jump_time_to_descent : float = 0.35

@onready var jump_velocity : float = ((2.0 * jump_height) / jump_time_to_peak) * -1.0
@onready var bounce_velocity : float = ((1.25 * jump_height) / jump_time_to_peak) * -1.0
@onready var float_velocity : float = ((jump_height) / jump_time_to_peak) * -1.0
@onready var jump_gravity : float = ((-2.0 * jump_height) / (pow(jump_time_to_peak, 2))) * -1.0
@onready var fall_gravity : float = ((-2.0 * jump_height) / (pow(jump_time_to_descent, 2))) * -1.0

signal jump_pressed

func apply_gravity(delta: float) -> void:
	actor.velocity.y += getGravity() * delta
	actor.velocity.y = min(actor.velocity.y, 250)

func getGravity() -> float:
	return jump_gravity if actor.velocity.y < 0 else fall_gravity

func getJumpVelocity() -> float:
	return jump_velocity

func getFloatingVelocity() -> float:
	return float_velocity

func getBounceVelocity() -> float:
	return bounce_velocity

func input_jump() -> void:
	if Input.is_action_just_pressed("jump"):
		jump_pressed.emit()
		actor.velocity.y = getJumpVelocity()

func float_jump() -> void:
	if Input.is_action_just_released("jump") and actor.velocity.y < (getJumpVelocity() / 2):
		actor.velocity.y = getFloatingVelocity()

func bounce_off_enemy() -> void:
	actor.velocity.y = getBounceVelocity()
