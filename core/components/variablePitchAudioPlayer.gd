class_name VariablePitchAudioStreamPlayer

extends AudioStreamPlayer

@export var pitch_min: float = 0.8
@export var pitch_max: float = 1.2
@export var auto_play_with_variance: = false


func _ready() -> void:
	if auto_play_with_variance:
		play_with_variance(0.0)


func play_with_variance(from_position: float = 0.0) -> void:
	pitch_scale = randf_range(pitch_min, pitch_max)
	play(from_position)


func _on_gravity_component_jump_pressed() -> void:
	play_with_variance()

