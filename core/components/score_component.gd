class_name ScoreComponent

extends Node

@export var game_stats: GameStats
@export var adjust_amount: int = 1

func adjust_score(amount: int = adjust_amount) -> void:
	game_stats.score += amount

func adjust_kills(amount: int = adjust_amount) -> void:
	game_stats.kills += amount

func resetStats() -> void:
	game_stats.score = 0
	game_stats.kills = 0
