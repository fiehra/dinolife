class_name AccelerationCompoent
extends Node

@export var actor: Node2D
@export var ACCELERATION: int = 600
@export var AIRACCELERATION: int = 300
@export var MAX_SPEED: int = 80
@export var DASH_SPEED: int = 133

func apply_acceleration(amount: float, delta: float) -> void:
	if actor.is_on_floor():
		actor.velocity.x = move_toward(actor.velocity.x, MAX_SPEED * amount, ACCELERATION * delta)
	else:
		actor.velocity.x = move_toward(actor.velocity.x, MAX_SPEED * amount, AIRACCELERATION * delta)

func apply_dash(amount: float, delta: float) -> void:
	if actor.is_on_floor():
		actor.velocity.x = move_toward(actor.velocity.x, DASH_SPEED * amount, ACCELERATION * delta)
	else:
		actor.velocity.x = move_toward(actor.velocity.x, DASH_SPEED * amount, AIRACCELERATION * delta)


