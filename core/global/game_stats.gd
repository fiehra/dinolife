class_name GameStats

extends Resource

@export var kills: int = 0 :
	set(value):
		kills = value
		kills_changed.emit(kills)

@export var score: int = 0 :
	set(value):
		score = value
		score_changed.emit(score)

@export var highscore: int = 0
@export var maxKills: int = 0

signal score_changed(new_score: int)

signal kills_changed(new_kills: int)

