extends Node

var maxMeatLevel1: int = 7
var collectedMeatLevel1: int = 0
var maxMeatLevel2: int = 7
var collectedMeatLevel2: int = 0
var currentLevel: int = 1
var currentMaxMeat: int = 7
var game_over_type: String = 'complete'
var time_elapsed := 0.0
var timerStopped := false
var playMusic := true
var playSFX := true
var deaths := 0

signal next_level_unlocked
signal level_changed(level: int)


func _process(delta: float) -> void:
	if !timerStopped:
		time_elapsed += delta


func _ready() -> void:
	currentLevel = 1


func increaseDeaths() -> void:
	deaths += 1


func resetDeaths() -> void:
	deaths = 0


func turnOnMusic() -> void:
	playMusic = true
	AudioServer.set_bus_volume_db(2, linear_to_db(1))


func turnOffMusic() -> void:
	playMusic = false
	AudioServer.set_bus_volume_db(2, linear_to_db(0))


func turnOnSFX() -> void:
	playSFX = true
	AudioServer.set_bus_volume_db(1, linear_to_db(1))


func turnOffSFX() -> void:
	playSFX = false
	AudioServer.set_bus_volume_db(1, linear_to_db(0))


func resetTimer() -> void:
	time_elapsed = 0.0
	timerStopped = false


func stopTimer() -> void:
	timerStopped = true


func set_current_level(level: int) -> void:
	currentLevel = level
	if (currentLevel == 1):
		currentMaxMeat = maxMeatLevel1
		level_changed.emit(1)
	elif (currentLevel == 2):
		currentMaxMeat = maxMeatLevel2
		level_changed.emit(2)


func collect_meat() -> void:
	if (currentLevel == 1):
		currentMaxMeat = 5
		collectedMeatLevel1 += 1
		if (collectedMeatLevel1 == maxMeatLevel1):
			next_level_unlocked.emit()
	elif (currentLevel == 2):
		currentMaxMeat = 7
		collectedMeatLevel2 += 1
		if (collectedMeatLevel2 == maxMeatLevel2):
			next_level_unlocked.emit()


func reset_level_stats() -> void:
	collectedMeatLevel1 = 0
	collectedMeatLevel2 = 0
	currentLevel = 1


