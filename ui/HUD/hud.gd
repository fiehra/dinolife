extends Node2D

@export var game_stats: GameStats
@onready var deaths_label: Label = $deaths/deaths_label
@onready var kills_label: Label = $kills/kills_label
@onready var score_label: Label = $kills/kills_label
@onready var stopwatch_label: Label = $HBoxContainer2/stopwatch_label
@onready var info_label: Label = $info_label
@onready var unlock_hint: Label = $unlock_hint
@onready var max_meat_label: Label = $HBoxContainer/max_meat_label
@onready var collected_meat_label: Label = $HBoxContainer/collected_meat_label


func _process(_delta: float) -> void:
	stopwatch_label.text = str(GameHelper.time_elapsed).pad_decimals(1)


func _ready() -> void:
	max_meat_label.text = str(GameHelper.currentMaxMeat)
	GameHelper.next_level_unlocked.connect(showUnlockHint)
	update_deaths_label(GameHelper.deaths)
	GameHelper.level_changed.connect(update_meat_label)
	game_stats.kills_changed.connect(update_kills_label)
	game_stats.score_changed.connect(update_meat_label)


func update_deaths_label(new_deaths: int) -> void:
	deaths_label.text = str(new_deaths)


func update_kills_label(new_kills: int) -> void:
	kills_label.text = str(new_kills)


func update_meat_label(collected_meat: int) -> void:
	collected_meat_label.text = str(collected_meat)


func _on_hide_info_timer_timeout() -> void:
	info_label.queue_free()


func showUnlockHint() -> void:
	unlock_hint.show()
	await get_tree().create_timer(2).timeout
	unlock_hint.hide()


