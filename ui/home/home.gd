extends Control


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("jump"):
		get_tree().change_scene_to_file("res://levels/level_1.tscn")
		GameHelper.set_current_level(1)
		GameHelper.reset_level_stats()
		GameHelper.resetTimer()
	elif Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
	elif Input.is_action_just_pressed("interact"):
		get_tree().change_scene_to_file("res://ui/settings/controls_ui.tscn")
