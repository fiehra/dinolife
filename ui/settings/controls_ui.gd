extends Control

@onready var music_off: Sprite2D = $HBoxContainer3/music_off
@onready var music_on: Sprite2D = $HBoxContainer3/music_on

func _ready() -> void:
	pass


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("dash") or Input.is_action_just_pressed("ui_cancel"):
		get_tree().change_scene_to_file("res://ui/home/home_ui.tscn")
	elif Input.is_action_just_pressed("interact"):
		if GameHelper.playSFX:
			GameHelper.turnOffSFX()
		else:
			GameHelper.turnOnSFX()
	elif Input.is_action_just_pressed("muteMusic"):
		if GameHelper.playMusic:
			GameHelper.turnOffMusic()
			music_off.show()
			music_on.hide()
		else:
			GameHelper.turnOnMusic()
			music_on.show()
			music_off.hide()
