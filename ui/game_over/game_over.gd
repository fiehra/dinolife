# game over ui script
extends Control

const SAVE_PATH = "user://savegame.cfg"

var save_path: String = SAVE_PATH


@export var game_stats: GameStats
@onready var score_component: ScoreComponent = $ScoreComponent
@onready var game_over_type_value: Label = $CenterContainer/VBoxContainer/gameOverTypeLabel
@onready var score_value: Label = $CenterContainer/VBoxContainer/HBoxContainer/scoreValue
@onready var kills_value: Label = $CenterContainer/VBoxContainer/HBoxContainer3/killsValue
@onready var dialog: AcceptDialog = $CenterContainer/AcceptDialog


func _ready() -> void:
	loadGameOverType()
	loadScore()
	dialog.confirmed.connect(goToHome)
	dialog.canceled.connect(closeDialog)


func goToHome() -> void:
	get_tree().change_scene_to_file("res://ui/home/home_ui.tscn")
	GameHelper.resetDeaths()
	score_component.resetStats()


func closeDialog() -> void:
	dialog.visible = false


func update_game_over_type(type: String) -> void:
	game_over_type_value.text = str(type)


func _process(_delta: float) -> void:
	if Input.is_action_just_pressed("jump") && !dialog.visible:
		GameHelper.resetTimer()
		# level complete
		if ((GameHelper.game_over_type == 'complete') and (GameHelper.currentLevel == 1)):
			get_tree().change_scene_to_file("res://levels/level_2.tscn")
			score_component.resetStats()
			GameHelper.set_current_level(2)
		elif ((GameHelper.game_over_type == 'complete') and (GameHelper.currentLevel == 2)):
			get_tree().change_scene_to_file("res://ui/home/home_ui.tscn")
			score_component.resetStats()
		# death
		elif (GameHelper.game_over_type == 'death' and GameHelper.currentLevel == 1):
			GameHelper.reset_level_stats()
			GameHelper.set_current_level(1)
			score_component.resetStats()
			get_tree().change_scene_to_file("res://levels/level_1.tscn")
		elif (GameHelper.game_over_type == 'death' and GameHelper.currentLevel == 2):
			GameHelper.reset_level_stats()
			GameHelper.set_current_level(2)
			score_component.resetStats()
			get_tree().change_scene_to_file("res://levels/level_2.tscn")
	elif Input.is_action_just_pressed("dash") or Input.is_action_just_pressed("ui_cancel"):
		if !dialog.visible and GameHelper.currentLevel == 2:
			dialog.visible = true
		elif dialog.visible:
			closeDialog()
		else:
			goToHome()





func loadScore() -> void:
	score_value.text = str(game_stats.score)
	kills_value.text = str(game_stats.kills)


func loadGameOverType() -> void:
	if ((GameHelper.game_over_type == 'complete') and (GameHelper.currentLevel == 1)):
		game_over_type_value.text = 'level 1 complete'
	elif ((GameHelper.game_over_type == 'complete') and (GameHelper.currentLevel == 2)):
		game_over_type_value.text = 'thank you for playing!'
	else:
		game_over_type_value.text = 'you died'

