extends CharacterBody2D

# state machine
enum {WALK, IDLE, JUMP, FALL, DASH, HURT, CHEER, DECAY}

var state := IDLE
var DIRECTION := 1
var isDead: bool = false

@export var game_stats: GameStats
@export var speed: = Vector2(400.0, 500.0)
@onready var gravity_component: GravityComponent = $components/GravityComponent
@onready var friction_component: FrictionComponent = $components/FrictionComponent
@onready var acceleration_component: AccelerationCompoent = $components/AccelerationCompoent
@onready var animatedSprite: = $AnimatedSprite2D
@onready var hurtbox: CollisionShape2D = $hurtbox/hurtbox
@onready var bounceSound: VariablePitchAudioStreamPlayer = $bounceSFX
@onready var deathSound: VariablePitchAudioStreamPlayer = $deathSFX


func _physics_process(delta: float) -> void:
	gravity_component.apply_gravity(delta)
	handle_animations()
	handle_movement(delta)
	handle_jump()


func handle_movement(delta: float) -> void:
	var input_axis := Input.get_axis("walk_left", "walk_right")
	if isDead:
		friction_component.apply_friction(delta)
	else:
		set_direction(input_axis)
		if input_axis == 0:
			state = IDLE
			friction_component.apply_friction(delta)
		elif input_axis != 0:
			if Input.is_action_pressed("dash"):
				state = DASH
				acceleration_component.apply_dash(input_axis, delta)
			else:
				state = WALK
				acceleration_component.apply_acceleration(input_axis, delta)
	move_and_slide()


func handle_jump() -> void:
	# DONT MOVE_AND_SLIDE HERE NUB
	if isDead:
		return
	else:
		if is_on_floor():
			gravity_component.input_jump()
		elif not is_on_floor():
			gravity_component.float_jump()
			state = JUMP



func handle_animations() -> void:
	match state:
		IDLE:
			animatedSprite.play("idle")
		WALK:
			animatedSprite.play("walk")
			animatedSprite.flip_h = velocity.x < 0
		DASH:
			animatedSprite.play("dash")
			animatedSprite.flip_h = velocity.x < 0
		JUMP:
			animatedSprite.play("jump")
			animatedSprite.flip_h = DIRECTION == -1
		FALL:
			animatedSprite.play("fall")
			animatedSprite.flip_h = DIRECTION == -1
		HURT:
			animatedSprite.play("hurt")
			animatedSprite.flip_h = DIRECTION == -1
			isDead = true
		CHEER:
			animatedSprite.play("cheer")
			animatedSprite.flip_h = DIRECTION == -1
			isDead = true
		DECAY:
			animatedSprite.play("decay")
			animatedSprite.flip_h = DIRECTION == -1
			isDead = true
		_: # default
			animatedSprite.play("idle")


func set_direction(direction: float) -> void:
	if direction == 1:
		DIRECTION = 1
	elif direction == -1:
		DIRECTION = -1
	else: return


func changeLevel() -> void:
	if is_inside_tree():
		get_tree().change_scene_to_file("res://ui/game_over/game_over_menu_ui.tscn")


func checkIfHazard(area: Area2D) -> bool:
	if  "spikes" in area.name || "skullSpikes" in area.name || "fire" in area.name:
		return true
	else:
		return false


func disable_collisions() -> void:
	hurtbox.set_deferred("disabled", true)
	set_collision_layer_value(2, false)
	set_collision_mask_value(6, false)


func startDeathTransition() -> void:
	GameHelper.game_over_type = 'death'
	GameHelper.stopTimer()
	GameHelper.increaseDeaths()
	disable_collisions()
	state = HURT
	deathSound.play_with_variance()
	await get_tree().create_timer(1.0).timeout
	state = DECAY
	await get_tree().create_timer(2.0).timeout
	call_deferred("changeLevel")
	queue_free()


func _on_stomp_hitbox_area_entered(area: Area2D) -> void:
	var isHazard: bool = checkIfHazard(area)
	if isHazard and state != CHEER:
		startDeathTransition()
	if "stompAreaHurtbox" in area.name:
		var enemy: Node2D = area.get_parent()
		# check for HURT (2) or DECAY (3) state before bouncing
		if enemy.state != 2 and enemy.state != 3:
			bounceSound.play_with_variance()
			gravity_component.bounce_off_enemy()


func _on_hurtbox_area_entered(area:Area2D) -> void:
	if area.name == "patrollerHitbox":
		var enemy: Node2D = area.get_parent()
		# check for HURT (2) or DECAY (3) state before dying
		if enemy.state != 2 and enemy.state != 3 and state != CHEER:
			startDeathTransition()
	elif "torchHitbox" in area.name:
		await get_tree().create_timer(0.3).timeout
		state = CHEER
		GameHelper.game_over_type = 'complete'
		await get_tree().create_timer(3.0).timeout
		call_deferred("changeLevel")
		queue_free()
