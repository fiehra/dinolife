extends Node2D

@onready var levelCompleteSound: VariablePitchAudioStreamPlayer = $levelCompleteSFX
@onready var torchHitbox: CollisionShape2D = $torchHitbox/CollisionShape2D
@onready var flameSprite: AnimatedSprite2D = $AnimatedSprite2D

func _ready() -> void:
	GameHelper.next_level_unlocked.connect(allMeatCollected)


func enable_torch() -> void:
	torchHitbox.set_deferred("disabled", false)
	flameSprite.show()


func allMeatCollected() -> void:
	enable_torch()


func _on_area_2d_area_entered(area:Area2D) -> void:
	if area.owner.name == "player":
		levelCompleteSound.play_with_variance()
		GameHelper.stopTimer()
