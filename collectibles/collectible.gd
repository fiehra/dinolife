extends Node2D

@export var pickup_amount: int = 1
@onready var score_component: ScoreComponent = $ScoreComponent
@onready var collectSound: VariablePitchAudioStreamPlayer = $collectSFX
@onready var meatSprite: AnimatedSprite2D = $AnimatedSprite2D
@onready var collectAreaCollision: CollisionShape2D = $collectArea/CollisionShape2D

func _on_collect_area_area_entered(area:Area2D) -> void:
	if area.owner.name == "player":
		collectSound.play_with_variance()
		score_component.adjust_score(1)
		GameHelper.collect_meat()
		disable_collisions()
		meatSprite.hide()
		await get_tree().create_timer(0.2).timeout
		if is_inside_tree():
			queue_free()


func disable_collisions() -> void:
	collectAreaCollision.set_deferred("disabled", true)
