extends Sprite2D

@onready var shakeArea: CollisionShape2D = $shakeArea/CollisionShape2D

func _on_shake_area_area_entered(area:Area2D) -> void:
	if area.owner.name == "player" or "patroller" in area.name:
		material.set_shader_parameter("shake_intensity", 1)
		await get_tree().create_timer(1.33).timeout
		material.set_shader_parameter("shake_intensity", 0)







