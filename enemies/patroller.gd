extends CharacterBody2D

# state machine
enum {WALK, IDLE, HURT, DECAY}

var state := WALK

var DIRECTION: int = 1
var SPEED: int = 50

@onready var score_component: ScoreComponent = $ScoreComponent
@onready var gravity_component: GravityComponent = $GravityComponent
@onready var animatedSprite: = $AnimatedSprite2D
@onready var ledgeCheckLeft: RayCast2D = $ledgeCheckLeft
@onready var ledgeCheckRight: RayCast2D = $ledgeCheckRight
@onready var stomp_area: Area2D = $stompAreaHurtbox
@onready var stompAreaCollision: CollisionShape2D = $stompAreaHurtbox/hurtbox


func _physics_process(delta: float) -> void:
	gravity_component.apply_gravity(delta)
	handle_animations()
	patrol()
	move_and_slide()


func patrol() -> void:
	if state == HURT:
		velocity.x = move_toward(velocity.x, 0, 1000)
	elif state == DECAY:
		velocity.x = 0
	else:
		var found_wall := is_on_wall()
		var found_ledge := not ledgeCheckLeft.is_colliding() or not ledgeCheckRight.is_colliding()
		if found_wall or found_ledge:
			DIRECTION *= -1

		if is_on_floor():
			state = WALK
			velocity.x = DIRECTION * SPEED


func handle_animations() -> void:
	match state:
		IDLE:
			animatedSprite.play("idle")
		HURT:
			animatedSprite.play("hurt")
			animatedSprite.flip_h = DIRECTION == -1
		DECAY:
			animatedSprite.play("decay")
			animatedSprite.flip_h = DIRECTION == -1
		WALK:
			animatedSprite.play("walk")
			animatedSprite.flip_h = velocity.x < 0
		_: # default
			animatedSprite.play("idle")


func disable_collisions() -> void:
	stompAreaCollision.set_deferred("disabled", true)
	set_collision_mask_value(2, false)
	set_collision_layer_value(6, false)


func _on_stomp_area_area_entered(area: Area2D) -> void:
	if state in [HURT, DECAY]: return
	if area.global_position.y < stomp_area.global_position.y:
		await get_tree().create_timer(0.1).timeout
		disable_collisions()
		state = HURT
		await get_tree().create_timer(1.0).timeout
		score_component.adjust_kills(1)
		state = DECAY
		await get_tree().create_timer(4.0).timeout
		queue_free()
